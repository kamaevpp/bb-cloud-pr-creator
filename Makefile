.venv:
	python3 -m venv .venv

.PHONY: install
install: .venv
	poetry install --no-dev

.PHONY: install-dev
install-dev: .venv
	poetry install

# tests
## linters
SOURCES = src/ tests/

.PHONY: lint/isort
lint/isort:
	poetry run isort --check $(SOURCES)

.PHONY: lint/black
lint/black:
	poetry run black --check --diff $(SOURCES)

.PHONY: lint/mypy
lint/mypy:
	MYPYPATH=src poetry run mypy --strict -p bb_cloud_pr_creator

.PHONY: lint/flake8
lint/flake8:
	poetry run flake8 $(SOURCES)

.PHONY: lint
lint: lint/isort lint/black lint/mypy lint/flake8

## autoformaters
.PHONY: format/isort
format/isort:
	poetry run isort $(SOURCES)

.PHONY: format/black
format/black:
	poetry run black $(SOURCES)

.PHONY: format
format: format/black format/isort

## unittests
.PHONY: unitests
unittests:
	PYTHONPATH=src poetry run coverage run -m pytest -v tests/
	poetry run coverage report

.PHONY: test
test: lint unittests

# deps
.PHONY: deps/update
deps/update:
	poetry update

.PHONY: deps/push2git
deps/push2git: MESSAGE ?= "fix: automatic update of dependencies [$(shell date '+%Y-%m-%d')]"
deps/push2git: BRANCH ?= "autoupdate-$(shell date '+%Y-%m-%d')"
deps/push2git:
	git add poetry.lock
	git commit -m $(MESSAGE)
	git push --force origin HEAD:$(BRANCH)

# versioning
.PHONY: version/bump
version/bump:
	poetry run semantic-release version
	git push --follow-tags

.PHONY: version/print
version/print:
	@poetry run semantic-release print-version --current

# run
.PHONY: run/pipe
run/pipe:
	PYTHONPATH=src poetry run python -m bb_cloud_pr_creator

# docker
.PHONY: docker/build
docker/build:
	docker build --file Dockerfile --tag kammala/bb-cloud-pr-creator:$(VERSION) .

.PHONY: docker/push
docker/push:
	docker login --username $(DOCKERHUB_USERNAME) --password $(DOCKERHUB_PASSWORD)
	docker push kammala/bb-cloud-pr-creator:$(VERSION)
