import http
import pathlib

import pytest
import responses

from bb_cloud_pr_creator import bitbucket

fixtures = pathlib.Path(__file__).parent / "fixtures"


class TestCreatePR:
    @responses.activate
    def test_base(self):
        # arange
        responses.add(
            method=responses.POST,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/pullrequests/",
            body=(fixtures / "create_pr_base.json").read_bytes(),
            adding_headers={"Content-Type": "application/json"},
        )

        # act
        resp = bitbucket.create_pr(
            repo="test/repo",
            auth=("test", "password"),
            source="branch",
            target=None,
            title="Title",
            reviewers=[],
            close_source_branch=True,
        )

        # assert
        expected_pr_href = (
            "https://bitbucket.org/kamaevpp/bb-cloud-pr-creator/pull-requests/2"
        )
        assert resp == expected_pr_href

    @responses.activate
    def test_full(self):
        # arange
        responses.add(
            method=responses.POST,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/pullrequests/",
            body=(fixtures / "create_pr_full.json").read_bytes(),
            adding_headers={"Content-Type": "application/json"},
        )

        # act
        resp = bitbucket.create_pr(
            repo="test/repo",
            auth=("test", "password"),
            source="branch",
            target="master",
            title="Title",
            reviewers=["{7dc7cb3f-019b-49a0-af46-ecba1e8253bb}"],
            close_source_branch=False,
        )

        # assert
        expected_pr_href = (
            "https://bitbucket.org/kamaevpp/bb-cloud-pr-creator/pull-requests/2"
        )
        assert resp == expected_pr_href

    @responses.activate
    def test_bad_request(self):
        # arange
        responses.add(
            method=responses.POST,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/pullrequests/",
            body=(fixtures / "create_pr_bad_request.json").read_bytes(),
            adding_headers={"Content-Type": "application/json"},
            status=http.HTTPStatus.BAD_REQUEST,
        )

        # act / assert
        with pytest.raises(RuntimeError) as ex:
            bitbucket.create_pr(
                repo="test/repo",
                auth=("test", "password"),
                source="branch",
                target="master",
                title="Title",
                reviewers=["{7dc7cb3f-019b-49a0-af46-ecba1e8253bb}"],
                close_source_branch=False,
            )

        assert "branch not found" in str(ex.value)

    @responses.activate
    def test_internal_server_error(self):
        # arange
        responses.add(
            method=responses.POST,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/pullrequests/",
            body=b"",
            adding_headers={"Content-Type": "application/json"},
            status=http.HTTPStatus.INTERNAL_SERVER_ERROR,
        )

        # act / assert
        with pytest.raises(RuntimeError) as ex:
            bitbucket.create_pr(
                repo="test/repo",
                auth=("test", "password"),
                source="branch",
                target="master",
                title="Title",
                reviewers=["{7dc7cb3f-019b-49a0-af46-ecba1e8253bb}"],
                close_source_branch=False,
            )

        assert "Failed to create PR" in str(ex.value)


class TestFetchReviewers:
    @responses.activate
    def test_ok(self):
        # arange
        responses.add(
            method=responses.GET,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/default-reviewers/",
            body=(fixtures / "fetch_reviewers_ok.json").read_bytes(),
            adding_headers={"Content-Type": "application/json"},
        )

        # act
        resp = bitbucket.fetch_default_reviewers(
            repo="test/repo",
            auth=bitbucket.Auth(name="test", password="password"),
        )

        # assert
        assert resp == ["{7dc7cb3f-019b-49a0-af46-ecba1e8253bb}"]

    @responses.activate
    def test_unauthorized(self):
        # arange
        responses.add(
            method=responses.GET,
            url="https://api.bitbucket.org/2.0/repositories/test/repo/default-reviewers/",
            body=b"",
            adding_headers={"Content-Type": "application/json"},
            status=http.HTTPStatus.UNAUTHORIZED,
        )

        # act / assert
        with pytest.raises(RuntimeError) as ex:
            bitbucket.fetch_default_reviewers(
                repo="test/repo",
                auth=bitbucket.Auth(name="test", password="password"),
            )

        assert "Failed to fetch default reviewers" in str(ex.value)
