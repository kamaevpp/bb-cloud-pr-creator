# Bitbucket Pipelines Pipe: PR creator for Bitbucket Cloud

Creates PR using Bitbucket API for specified branch using provided credentials

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: kamaevpp/bb-cloud-pr-creator:v0.1.6
  variables:
    SOURCE_BRANCH: "my-branch"
    TARGET_BRANCH: "master"
    TITLE: "Automatic PR"
    BITBUCKET_AUTH: "repo_owner:app_password"
    # DEBUG: "true" # Optional
```
## Variables

| Variable                       | Usage |
| ------------------------------ | ----- |
| SOURCE_BRANCH (*)              | Source branch for PR |
| TARGET_BRANCH                  | Target branch for PR. Default: default repo branch |
| TITLE (*)                      | PR title |
| BITBUCKET_AUTH (*)             | Username and app password which will be used for PR creation. Requires `pullrequests:write` permissions to be granted. |
| DEBUG                          | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by kamaevpp@gmail.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
