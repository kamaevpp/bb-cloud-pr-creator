import logging
from typing import Optional

import pydantic

from bb_cloud_pr_creator import bitbucket

logger = logging.getLogger(__name__)


class Settings(pydantic.BaseSettings):
    source_branch: str
    target_branch: Optional[str] = None
    title: str
    bitbucket_auth: str
    bitbucket_repo_full_name: str
    close_source_branch: bool = True
    debug: bool = False


def run(settings: Settings) -> None:
    auth = bitbucket.Auth(*settings.bitbucket_auth.split(":", 1))

    logger.info(
        f"Creating PR[{settings.title}] from {settings.source_branch} to "
        f"{settings.target_branch or 'default repo branch'}"
    )

    try:
        logger.debug("Fetching default reviewers")
        reviewers = bitbucket.fetch_default_reviewers(
            repo=settings.bitbucket_repo_full_name,
            auth=auth,
        )

        logger.debug("Creating actual PR")
        pr_href = bitbucket.create_pr(
            repo=settings.bitbucket_repo_full_name,
            auth=auth,
            title=settings.title,
            source=settings.source_branch,
            target=settings.target_branch,
            reviewers=reviewers,
            close_source_branch=settings.close_source_branch,
        )
        logger.info(f"PR created: {pr_href}")

    except Exception as ex:
        logger.info("Failed to proceed", exc_info=ex)
        raise RuntimeError(f"Failed to proceed: {ex}") from ex
