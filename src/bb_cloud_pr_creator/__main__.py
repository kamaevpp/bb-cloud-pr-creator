import logging

from bb_cloud_pr_creator import pipe

if __name__ == "__main__":
    settings = pipe.Settings()
    logging.basicConfig(level=logging.INFO if not settings.debug else logging.DEBUG)
    pipe.run(settings)
