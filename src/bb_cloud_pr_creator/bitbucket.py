import http
import logging
from typing import NamedTuple, Optional

import requests

logger = logging.getLogger(__name__)


class Auth(NamedTuple):
    name: str
    password: str


def fetch_default_reviewers(*, repo: str, auth: Auth) -> list[str]:
    resp = requests.get(
        url=f"https://api.bitbucket.org/2.0/repositories/{repo}/default-reviewers/",
        auth=auth,
    )
    try:
        resp.raise_for_status()
    except requests.HTTPError as ex:
        raise RuntimeError("Failed to fetch default reviewers") from ex

    return [
        reviewer["uuid"]
        for reviewer in resp.json()["values"]
        if reviewer["nickname"] != auth.name
    ]


def create_pr(
    *,
    repo: str,
    auth: Auth,
    source: str,
    target: Optional[str],
    title: Optional[str],
    reviewers: list[str],
    close_source_branch: bool,
) -> str:
    data = {
        "source": {"branch": {"name": source}},
        "close_source_branch": close_source_branch,
        "title": title,
    }

    if target:
        data["destination"] = {"branch": {"name": target}}

    if reviewers:
        data["reviewers"] = [{"uuid": reviewer} for reviewer in reviewers]

    logger.debug("Actual request: %s", data)
    resp = requests.post(
        url=f"https://api.bitbucket.org/2.0/repositories/{repo}/pullrequests/",
        auth=auth,
        json=data,
    )

    if resp.status_code == http.HTTPStatus.BAD_REQUEST:
        logger.debug("Actual response: %s", resp.json())
        raise RuntimeError(f"Improper request: {resp.json()['error']['message']}")

    try:
        resp.raise_for_status()
    except requests.HTTPError as ex:
        raise RuntimeError("Failed to create PR") from ex

    return resp.json()["links"]["html"]["href"]  # type: ignore
