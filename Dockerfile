FROM python:3.9-slim

RUN apt-get update \
    && apt-get install make \
    && apt-get clean && rm -rf /var/cache/apt \
    && pip install poetry

COPY Makefile /app/
COPY pyproject.toml /app/
COPY poetry.lock /app/

WORKDIR /app/

RUN make install

COPY src /app/
COPY LICENSE.txt pipe.yml README.md /app/

ENTRYPOINT ["make", "-C", "/app", "run/pipe"]
